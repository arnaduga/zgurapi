const application = require("./app");

application.fastify.log.info(`*****************************************************************************`);
application.fastify.log.info(`SERVER STARTED on PORT ${application.PORT}`);
application.fastify.log.info(`-----------------------------------------------------------------------------`);
application.fastify.log.info(`MAX rate calls     : ${process.env.ZGURAPI_RATE_MAX} calls by second`);
application.fastify.log.info(`Log level          : ${process.env.ZGURAPI_LOG_LEVEL || "debug"}`);
