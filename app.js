require("dotenv").config();

const { version } = require("moment");
var moment = require("moment");

const { global } = require("./global.js");
const keys = require("./keys");

// Environment constants
const PORT = process.env.ZGURAPI_PORT || 5000;
const HOST = "0.0.0.0";

const unauthenticatedPath = new Set(["/"]);

//Fastify object, CORE of HTTP API SERVICE
const fastify = require("fastify")({
  disableRequestLogging: true,

  logger: {
    level: process.env.ZGURAPI_LOG_LEVEL || "debug",
    timestamp: () => `,"time":"${new Date(Date.now()).toISOString()}"`,
    prettyPrint: true,
    base: null,
  },
});

// RATE LIMIT to protect backend
fastify.register(require("fastify-rate-limit"), {
  max: process.env.ZGURAPI_RATE_MAX || 100,
  timeWindow: "1 second",
});

fastify.addHook("onResponse", (request, reply) => {
  fastify.log.info(
    `[Id ${request.id.toString().padStart(6, "0")}]: in ${reply.getResponseTime().toFixed(3)} ms // http/${
      reply.statusCode
    }`
  );
});

fastify.addHook("onRequest", (request, reply, done) => {
  fastify.log.info(`[Id ${request.id.toString().padStart(6, "0")}]: Query ${request.method} ${request.url}`);

  var authNkey = "";
  var alreadySent = false;

  if (unauthenticatedPath.has(request.url)) {
    alreadySent = true;
    done();
    return;
  } else if (request.headers["x-api-key"]) {
    authNkey = request.headers["x-api-key"];
  } else if (request.query.apikey) authNkey = request.query.apikey;

  if (!authNkey && !alreadySent) {
    reply.statusCode = 401;
    reply.send({ statusCode: reply.statusCode, error: "Unauthorized", message: "The key is missing" });
  } else if (!keys.has(authNkey)) {
    reply.statusCode = 403;
    reply.send({
      statusCode: reply.statusCode,
      error: "Forbidden",
      message: "You don't have the appropriate rights",
    });
  } else {
    done();
  }
});

// Better handling response and log in case of rate limit reached
fastify.setErrorHandler(function (error, request, reply) {
  if (reply.statusCode === 429) {
    error.message = "You hit the rate limit! Slow down please!";
  }
  reply.send(error);
});

//CORS plugin
fastify.register(require("fastify-cors"), {});

// Load some routes
fastify.register(require("./routes/timeRoutes"));
fastify.register(require("./routes/systemRoutes"));
fastify.register(require("./routes/generatorRoutes"));

// Run the server!
fastify.listen(PORT, HOST, function (err, address) {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
});

// Graceful termination, just in case.
process.on("SIGINT", function () {
  fastify.log.info("Caught interrupt signal. Gracefully stop the application. Bye!");
  process.exit(0);
});

module.exports = { PORT, HOST, fastify };
