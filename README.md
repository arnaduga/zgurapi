# Zgur API

Set of utilities API for different purposes. Started when I needed a specific behavior for an IFTTT recipe :)

I run it on a  Raspberry Pi.

## How to use?

### Pre-requisites

To install and use this API, you need:
- [NodeJS](https://nodejs.org) installed. I used version `10.23`.
- [npm](https://npmjs.org)
- [git](https://git-scm.com/)


### Installation

On the computer/RaspberryPi, here is the walkthrough:
1. Go to the folder you want to store the application
1. Clone the repository: `git clone https://gitlab.com/arnaduga/zgurapi.git`
1. Go to the clones folder: `cd zgurapi`
1. Install dependencies: `npm install`
1. Copy the template KEYS file: `cp keys.template.js keys.js` 
1. Edit the `keys.js` by removing `"a"` and `"b"`, and adding the keys you want
1. Copy the template CONFIG file: `cp config.template.js config.js` 
1. Edit the `config.js` 

The API runs on port `5000`, except if you set an environment variable named `ZGURAPI_PORT`

> Note: to generate a random string to be used as key, you can use `openssl rand -base64 32`

### Make it daemon

To be sure the API is restarted after a reboot, you can use the handy [`pm2`](http://pm2.keymetrics.io/).

1. Install `pm2` with command `npm install -g pm2`
1. Prepare `pm2` to manage reboot: `pm2 startup`
1. Start your API: `pm2 start server.js --name ZgurAPI -i 2`. This command will launch 2 load-balanced instances of your app
1. Save the state: `pm2 save`

That should be good!

To check the status, type `pm2 ls`:
```console
pi@raspberrypi:~ $ pm2 ls
┌─────┬────────────┬─────────────┬─────────┬─────────┬──────────┬────────┬──────┬───────────┬──────────┬──────────┬──────────┬──────────┐
│ id  │ name       │ namespace   │ version │ mode    │ pid      │ uptime │ ↺    │ status    │ cpu      │ mem      │ user     │ watching │
├─────┼────────────┼─────────────┼─────────┼─────────┼──────────┼────────┼──────┼───────────┼──────────┼──────────┼──────────┼──────────┤
│ 0   │ ZgurAPI    │ default     │ 1.0.0   │ cluster │ 548      │ 14h    │ 0    │ online    │ 0%       │ 47.4mb   │ pi       │ disabled │
│ 1   │ ZgurAPI    │ default     │ 1.0.0   │ cluster │ 549      │ 14h    │ 0    │ online    │ 0%       │ 47.6mb   │ pi       │ disabled │
└─────┴────────────┴─────────────┴─────────┴─────────┴──────────┴────────┴──────┴───────────┴──────────┴──────────┴──────────┴──────────┘
pi@raspberrypi:~ $
```


To check logs of teh application, type `pm2 log`:
```console
/home/pi/.pm2/logs/ZgurAPI-out-0.log last 15 lines:
0|ZgurAPI  | [2021-01-23T08:43:10.934Z] INFO	 (548 on raspberrypi): incoming request
0|ZgurAPI  |     req: {
0|ZgurAPI  |       "method": "GET",
0|ZgurAPI  |       "url": "/version",
0|ZgurAPI  |       "hostname": "api.zgur.net",
0|ZgurAPI  |       "remoteAddress": "192.168.1.xxx",
0|ZgurAPI  |       "remotePort": 41300
0|ZgurAPI  |     }
0|ZgurAPI  |     reqId: 13
0|ZgurAPI  | [2021-01-23T08:43:10.936Z] INFO	 (548 on raspberrypi): request completed
0|ZgurAPI  |     res: {
0|ZgurAPI  |       "statusCode": 401
0|ZgurAPI  |     }
0|ZgurAPI  |     responseTime: 1.6218129992485046
0|ZgurAPI  |     reqId: 13

/home/pi/.pm2/logs/ZgurAPI-out-1.log last 15 lines:
1|ZgurAPI  | [2021-01-23T09:38:54.789Z] INFO	 (549 on raspberrypi): incoming request
1|ZgurAPI  |     req: {
1|ZgurAPI  |       "method": "GET",
1|ZgurAPI  |       "url": "/",
1|ZgurAPI  |       "hostname": "api.zgur.net",
1|ZgurAPI  |       "remoteAddress": "192.168.1.xxx",
1|ZgurAPI  |       "remotePort": 41390
1|ZgurAPI  |     }
1|ZgurAPI  |     reqId: 14
1|ZgurAPI  | [2021-01-23T09:38:54.793Z] INFO	 (549 on raspberrypi): request completed
1|ZgurAPI  |     res: {
1|ZgurAPI  |       "statusCode": 401
1|ZgurAPI  |     }
1|ZgurAPI  |     responseTime: 2.087998002767563
1|ZgurAPI  |     reqId: 14

pi@raspberrypi:~ $
```

### Update

If you need to update the code, go to your server in the code folder and type: `git pull`.

As the `config.js` file and `keys.js` file are not in the repo, it will keep you setup and keys.

> **WARNING**: in case of MAJOR change, some additional key may be required to be added into your config file. Please compare files and read the doc.

Restart the service by typing `pm2 restart ZgurAPI` if you used `pm2`.


## API endpoints

Documentation is in the OpenAPI Specification file: `./specifications/doc.yaml`.


### Authentication

To make it as compliant as possible with basic automation tools (IFTTT for instance), the authentication key can be sent as `header` or `queryString`.

Header key is `Authorization`, with a value `Apikey YouKey`.
Query string parameter name: `apikey`

May you send 2 different value at the same time for both header and querystring, the header one only will be considered.

For instance, a `cUrl`call:
```console
curl --location --request GET 'http://localhost:5000/version' --header 'Authorization: Apikey frDrEtf5fdfg6'
```