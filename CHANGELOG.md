# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.4]

## Added

- Gitlab CI file

## Changed

- Replace apikey header from `Authorization: apikey <apikey>` to `x-api-key: <apikey>` for better compliancy
- Update doc

## [0.3.x - 2021-02-01]

### Added

- `/health` endpoint
- `systemRoute` for system routes
- `GET /` empty response for API monitoring (UptimeRobot)
- `GET /generators/uuid` to generate UUID v4 only (for the moment)

### Changed 

- OpenAPI Specifications
- Errors 401/403 content

## [0.2.1 - 2021-01-23]

### Added

- `README.md` file
- `CHANGELOG.md` file
- `/version` endpoint
- `config.js` file, to externalise configuration
- `global.js` file 

### Changed

- Renamed `keys_template.js` to `keys.template.js` for better readability
- `apikey` value possible in header too.
- `apikey` to be sent in `Authorization` header


## [0.1.0] - 2021-01-21

### Added

- Initial commit
- `/longtime` endpoint
