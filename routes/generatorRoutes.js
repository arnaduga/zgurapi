// REQUIREMENTS

const { v4: uuidv4, v5: uuidv5 } = require("uuid");
var randomNumber = require("random-number-csprng");

// CONST for easy setup
const MAX_COUNT = 1000; // PASSWORD: Max size of a password
const LOREM_MAX = 1500; // LOREM: Max number of paragraph to generate
const LOREM_FORMATS = new Set(["html", "json", "text"]);

function loremSerializer(data, format) {
  let serialized = { type: null, body: null };

  if (format == "json") {
    serialized.type = "application/json";
    serialized.body = data;
    return serialized;
  }
  if (format == "text") {
    let strRes = "";
    for (let i = 0; i < data.length; i++) {
      strRes += data[i] + (i == data.length - 1 ? "" : "\n\n");
    }
    serialized.type = "text/plain";
    serialized.body = strRes;
    return serialized;
  }
  if ((format = "html")) {
    let strRes = "";
    for (let i = 0; i < data.length; i++) {
      strRes += `<p>${data[i]}</p>`;
    }
    serialized.type = "text/html";
    serialized.body = strRes;
    return serialized;
  }
}

async function routes(fastify, options) {
  fastify.get("/loremipsum", async (request, reply) => {
    const count = request.query.count || 1;
    const format = (request.query.format || "json").toLowerCase();

    if (!LOREM_FORMATS.has(format)) {
      fastify.log.warn(`Format not valid (input=${format})`);
      reply.statusCode = 400;
      reply.send({
        statusCode: 400,
        error: "Requested format is not valid",
        description: `You requested a format not allowed. Possible values: ${Array.from(LOREM_FORMATS)}`,
      });
      return;
    }

    if (count < 1 || count > LOREM_MAX) {
      fastify.log.warn(`Count value not authorized: min(default)=1, input={${count}}, max={${LOREM_MAX}}}`);
      reply.statusCode = 400;
      reply.send({
        statusCode: 400,
        error: "Count value unauthorized",
        description: `Paragraph count value outside authorized range ( 1 (default) <= count < ${LOREM_MAX})`,
      });
      return;
    }

    //Load data (no need to be done before checks)
    let loremData = [];
    try {
      loremData = require("./loremData.json");
    } catch (e) {
      loremData = ["Lorem ipsum dolor sit amet", "Praesent sit amet maximus lectus"];
      fastify.log.warn(`The LOREM IPSUM file was not correctly loaded: ${e.message}`);
    }

    fastify.log.info(`Data: ${loremData.length} possible items`);

    let res = [];
    for (var i = 0; i < count; i++) {
      res.push(loremData[i % loremData.length]);
    }

    let serialized = loremSerializer(res, format);
    reply.type(serialized.type);
    reply.send(serialized.body);
    return;

    //return { "it's": "all good", data: res };
  });

  fastify.get("/generators/password", async (request, reply) => {
    // Parameters and options
    const pwdLength = request.query.length || 8;

    // For
    const easyPwd = true;

    const baseCons = "bcdfghjklmnpqrstvwxz";
    const baseVoy = "aeiouy";

    let res = "";

    while (res.length < pwdLength) {
      if (easyPwd) {
        let syl1 = baseCons[await randomNumber(0, baseCons.length - 1)];
        let syl2 = baseVoy[await randomNumber(0, baseVoy.length - 1)];
        if ((await randomNumber(0, 1)) == 1) syl1 = syl1.toUpperCase();
        if ((await randomNumber(0, 1)) == 1) syl2 = syl2.toUpperCase();
        let syl = syl1 + syl2;
        res += syl;
      }
    }

    return {
      status: "ok",
      result: res,
    };
  });

  fastify.get("/generators/uuid", async (request, reply) => {
    // Possible parameters, defaulted
    const numberToGenerate = request.query.count || 5;
    const version = request.query.version || "v4";

    // Nope, that's too many
    if (numberToGenerate > MAX_COUNT) {
      reply.statusCode = 400;
      return {
        statusCode: 400,
        error: "Too big",
        message: `Set a lower value. Max: ${MAX_COUNT}`,
      };
    }

    // As-of-now, only v4
    if (version != "v4") {
      reply.statusCode = 400;
      return {
        statusCode: 400,
        error: "v5 only",
        message: `Set the (optional) version to v4`,
      };
    }

    var result = new Set();

    // Let's loop to fill this Set with uuids
    while (Array.from(result).length < numberToGenerate) {
      result.add(uuidv4());
    }

    return {
      version: "v4",
      values: Array.from(result),
    };
  });
}

module.exports = routes;
