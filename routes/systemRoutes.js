var moment = require("moment");
const { global } = require("../global.js");

const uptimeStart = moment();
const version = {
  name: "zgurapi",
  version: global.version,
  date: global.date,
};

async function routes(fastify, options) {
  fastify.get("/", async (request, reply) => {
    reply.send({});
  });

  fastify.get("/version", async (request, reply) => {
    reply.send(version);
  });

  fastify.get("/health", async (request, reply) => {
    var uptime = moment.duration(process.uptime(), "seconds");

    return {
      status: "ok",
      version: version,
      uptime: {
        startDate: uptimeStart.toISOString(),
        humanized: uptimeStart.fromNow(),
      },
    };
  });
}

module.exports = routes;
