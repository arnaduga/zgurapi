const DELAY_MAX_IN_SECONDS = 1800; //=30 minutes

var moment = require("moment");

async function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

async function routes(fastify, options) {
  fastify.get("/longtime", async (request, reply) => {
    // WAIT for a queryString parameter delay in seconds.

    fastify.log.info(`/slow-response called with delay parameter=${JSON.stringify(request.query)}`);

    const startRequest = new Date(Date.now()).toISOString();

    if (!request.query.delay) {
      fastify.log.error(`Missing delay parameter.`);
      reply.statusCode = 400;
      reply.send({
        statusCode: 400,
        error: "Missing parameter",
        description: `Missing delay parameter, in second`,
      });
      return;
    }

    const delay = moment.duration(request.query.delay);

    if (request.query.delay > DELAY_MAX_IN_SECONDS) {
      fastify.log.error(`Delay too important`);
      reply.statusCode = 400;
      reply.send({
        statusCode: 400,
        error: "Too long",
        description: `Delay value over the authorized limit (=${DELAY_MAX_IN_SECONDS})`,
      });
      return;
    }

    var tempTime = moment.duration(request.query.delay * 1000);
    fastify.log.info(
      `Waiting for ${tempTime.hours()} hours ${tempTime.minutes()} minutes and  ${tempTime.seconds()} seconds`
    );

    await sleep(request.query.delay * 1000);

    const endRequest = new Date(Date.now()).toISOString();

    return {
      status: "ok",
      delay: request.query.delay,
      time: { from: startRequest, to: endRequest },
    };
  });
}

module.exports = routes;
